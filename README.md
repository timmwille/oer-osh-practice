Open Source Hardware in Practice
===

## General

This module shows you how open source hardware works in practice.

Find a summarising module description [here](module-description.md) and a more detailed description below.

Slides rely on [reveal.js](https://revealjs.com/) and are written in markdown.

To launch them in a presentable mode just copy the markdown source into a new document of any instance of HedgeDoc (e.g. [this one](https://demo.hedgedoc.org/)).


## In Class

Find the slides here:

- [slide show](https://md.opensourceecology.de/p/u_9h-0LWm#/)
- [source](slides/S-OSH-Practice.md)

### Structure

**roughly:**

- Intro with short story about X
- Timm's Homework Block
- Role Play on general misconceptions
  - Licensing
  - Version Control
- The Real World:
  - DIN (Amelie)
  - Project (OSH-Spectrometer, Alessandro)
- closing Q&A

**time plan:**

| block                       | time | time cumulated | until hour |
|-----------------------------|------|----------------|------------|
| -                           | 0    | 0              | 14:00:00   |
| Intro (+ arriving buffer)   | 15   | 15             | 14:15:00   |
| Timm Homework Block         | 45   | 60             | 15:00:00   |
| Schedule                    | 5    | 65             | 15:05:00   |
| Role Play - Licensing       | 15   | 80             | 15:20:00   |
| Role Play - Version Control | 30   | 110            | 15:50:00   |
| *break*                     | 25   | 135            | 16:15:00   |
| DIN - Amelie                | 45   | 180            | 17:00:00   |
| DIN SPEC 3105               | 15   | 195            | 17:15:00   |
| Guest - Alessandro          | 30   | 225            | 17:45:00   |
| Q&A + Outro                 | 15   | 240            | 18:00:00   |

## Contributing

### Point of contact

Giving feedback or contributing to the material directly e.g. via merge request is always highly appreciated. This is what keeps the thing rolling.

In case you want to get in touch with the core team of maintainers, for the moment the easiest would be to simply drop an issue in which you explain your motivation for the contact. Happy to e-meet you then :)

### File name convention

It's fairly easy:

[type]-[module]-[sub-module]

S – Slides
G – Group Action

EXAMPLE 1:

`S-Intro-OSH.md` = slides for the Introduction module, specifically covering OSH

EXAMPLE 2:

`G-deep-value.md` = description for group action within the [Deep Dive module](#deep-dive), specifically the value creation exercise
